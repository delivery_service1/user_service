package config

import "errors"

var(
	ErrInvalidAuth = errors.New("invalid login or password")
)