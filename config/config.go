package config

import (
	"fmt"
	"os"
	"github.com/spf13/cast"
	"github.com/joho/godotenv"
)

type Config struct {
	PostgresHost     string
	PostgresPort     string
	PostgresUser     string
	PostgresPassword string
	PostgresDB       string

	ServiceName string
	Environment string
	LoggerLevel string

	ServiceGrpcHost string
	ServiceGrpcPort string

	OrderServiceGrpcHost string
	OrderServiceGrpcPort string
}

func Load() Config {
	err := godotenv.Load()
	if err != nil{
		fmt.Println("Error while loading from godotenv!",err.Error())
	}

	cfg := Config{}

	cfg.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST","localhost"))
	cfg.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT","5432"))
	cfg.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER","your_user_name"))
	cfg.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "your_password"))
	cfg.PostgresDB = cast.ToString(getOrReturnDefault("POSTGRES_DB","database"))

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME","service_name"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))
	cfg.LoggerLevel = cast.ToString(getOrReturnDefault("LOGGER_LEVEL", "debug"))

	cfg.ServiceGrpcHost = cast.ToString(getOrReturnDefault("GRPC_HOST", "localhost"))
	cfg.ServiceGrpcPort = cast.ToString(getOrReturnDefault("GRPC_PORT", ":8080"))

	cfg.OrderServiceGrpcHost = cast.ToString(getOrReturnDefault("ORDER_GRPC_HOST", "localhost"))
	cfg.OrderServiceGrpcPort = cast.ToString(getOrReturnDefault("ORDER_GRPC_PORT", ":8080"))

	return cfg
}

func getOrReturnDefault (key string, defaultValue interface{})interface{}{
	value := os.Getenv(key)
	if value != ""{
		return value
	}
	return defaultValue
}