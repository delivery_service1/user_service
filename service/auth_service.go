package service

import (
	"context"
 	"fmt"
 	pb "user_service/genproto/user_protos"
	"user_service/grpc/client"
	"user_service/pkg"
	"user_service/storage"
)

type authService struct {
	storage  storage.IStorage
	services client.IServiceManager
 	pb.UnimplementedAuthServiceServer
}

func NewAuthService(strg storage.IStorage, services client.IServiceManager) *authService {
	return &authService{
		storage:  strg,
		services: services,
 	}
}

func (a *authService) ClientLogin(ctx context.Context, req *pb.UserLoginRequest) (*pb.UserLoginResponse, error) {
	client, err := a.storage.Client().GetClientByCredentials(ctx, req)
	if err != nil {
		fmt.Println("Error while getting client's credentials!", err.Error())
 		return nil, err
	}

	err = pkg.CheckPassword(ctx, client.GetPassword(),req.Password)
	if err != nil{
		fmt.Println("Password doesn't match!", err.Error())
		return nil, err
	}

	return &pb.UserLoginResponse{
		Id:    client.GetId(),
		Phone: client.GetPhone(),
	}, nil
}

func (a *authService) AdminLogin(ctx context.Context, req *pb.UserLoginRequest) (*pb.UserLoginResponse, error) {
	admin, err := a.storage.Admin().GetAdminByCredentials(ctx, req)
	if err != nil {
		fmt.Println("Error while getting admin's credentials!", err.Error())
 		return nil, err
	}

	err = pkg.CheckPassword(ctx, admin.GetPassword(),req.Password)
	if err != nil{
		fmt.Println("Password doesn't match!", err.Error())
		return nil, err
	}

	return &pb.UserLoginResponse{
		Id:    admin.GetId(),
		Phone: admin.GetPhone(),
	}, nil
}

func (a *authService) CourierLogin(ctx context.Context, req *pb.UserLoginRequest) (*pb.UserLoginResponse, error) {
	courier, err := a.storage.Courier().GetCourierByCredentials(ctx, req)
	if err != nil {
		fmt.Println("Error while getting courier's credentials!", err.Error())
 		return nil, err
	}

	err = pkg.CheckPassword(ctx, courier.GetPassword(),req.Password)
	if err != nil{
		fmt.Println("Password doesn't match!", err.Error())
		return nil, err
	}

	return &pb.UserLoginResponse{
		Id:    courier.GetId(),
		Phone: courier.GetPhone(),
	}, nil
}

