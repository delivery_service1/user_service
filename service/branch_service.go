package service

import (
	"context"
	"fmt"
	pbo "user_service/genproto/order_protos"
	pb "user_service/genproto/user_protos"
	"user_service/grpc/client"
	"user_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type branchService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedBranchServiceServer
}

func NewBranchService(strg storage.IStorage, services client.IServiceManager) *branchService {
	return &branchService{
		storage: strg,
		serivces: services,
	}
}

func (b *branchService) Create(ctx context.Context, request *pb.CreateBranchRequest) (*pb.Branch, error) {
	_, err := b.serivces.DeliveryTarif().Get(ctx,&pbo.DeliveryTarifPrimaryKey{Id: request.DeliveryTarifId})
	if err != nil{
		fmt.Println("Error in service, while getting delivery tarif by id!", err.Error())
		return nil,err
	}

	branch, err := b.storage.Branch().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating branch!", err.Error())
		return nil, err
	}
	return branch, nil
}

func (b *branchService) Get(ctx context.Context, request *pb.BranchPrimaryKey) (*pb.Branch, error) {
	branch, err := b.storage.Branch().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting branch!",err.Error())
		return nil, err
	}
	return branch, nil
}

func (b *branchService) GetList(ctx context.Context, request *pb.GetBranchListRequest) (*pb.BranchesResponse, error) {
	branches, err := b.storage.Branch().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting branches list!", err.Error())
		return nil, err
	}
	return branches, nil
}

func (b *branchService) Update(ctx context.Context, request *pb.UpdateBranch) (*pb.Branch, error) {
	_, err := b.serivces.DeliveryTarif().Get(ctx,&pbo.DeliveryTarifPrimaryKey{Id: request.DeliveryTarifId})
	if err != nil{
		fmt.Println("Error in service, while getting delivery tarif by id!", err.Error())
		return nil,err
	}

	branch, err := b.storage.Branch().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating branch!", err.Error())
		return nil, err
	}
	return branch, nil
}

func (b *branchService) Delete(ctx context.Context, key *pb.BranchPrimaryKey) (*emptypb.Empty, error) {
	err := b.storage.Branch().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting branch!", err.Error())
		return nil,err
	}
	return nil,nil
}

func (b *branchService) GetActiveBranchesList(ctx context.Context, request *pb.GetBranchListRequest) (*pb.BranchesResponse, error) {
	branches, err := b.storage.Branch().GetActiveBranchesList(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while getting active branches!", err.Error())
		return nil,err
	}

	return branches, nil
}

