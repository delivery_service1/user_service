package service

import (
	"context"
	"fmt"
	pb "user_service/genproto/user_protos"
	"user_service/grpc/client"
	"user_service/pkg"
	"user_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type adminService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedAdminServiceServer
}

func NewAdminService(strg storage.IStorage, services client.IServiceManager) *adminService {
	return &adminService{
		storage: strg,
		serivces: services,
	}
}

func (a *adminService) Create(ctx context.Context, request *pb.CreateAdminRequest) (*pb.Admin, error) {
	hashedPassword, err := pkg.HashPassword(request.GetPassword())
	if err != nil{
		fmt.Println("Error in service, while hashing admins password!",err.Error())
		return nil, err
	}

	request.Password = hashedPassword

	admin, err := a.storage.Admin().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating admin!", err.Error())
		return nil, err
	}
	return admin, nil
}

func (a *adminService) Get(ctx context.Context, request *pb.AdminPrimaryKey) (*pb.Admin, error) {
	admin, err := a.storage.Admin().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting admin!",err.Error())
		return nil, err
	}
	return admin, nil
}

func (a *adminService) GetList(ctx context.Context, request *pb.GetAdminListRequest) (*pb.AdminsResponse, error) {
	categories, err := a.storage.Admin().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting admins list!", err.Error())
		return nil, err
	}
	return categories, nil
}

func (a *adminService) Update(ctx context.Context, request *pb.UpdateAdmin) (*pb.Admin, error) {
	admin, err := a.storage.Admin().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating admin!", err.Error())
		return nil, err
	}
	return admin, nil
}

func (a *adminService) Delete(ctx context.Context, key *pb.AdminPrimaryKey) (*emptypb.Empty, error) {
	err := a.storage.Admin().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting admin!", err.Error())
		return nil,err
	}
	return nil,nil
}

func (a *adminService) UpdatePassword(ctx context.Context, request *pb.ChangeAdminPasswordRequest) (*pb.AdminHashedPasswordResponse,error) {
	hashedPassword,err := a.storage.Admin().GetPassword(ctx, request)
	if err != nil{
		fmt.Println("Error in service, login is wrong!",err.Error())
	}

	err = pkg.CheckPassword(ctx, hashedPassword, request.OldPassword)
	if err != nil{
		fmt.Println("Error in service, password doesn't match!", err.Error())
		return nil,err
	}
	
	hashPassword, err := pkg.HashPassword(request.NewPassword)
	if err != nil{
		fmt.Println("Error in service, while hashing password!", err.Error())
		return nil,err
	}

	newPassword, err := a.storage.Admin().UpdatePassword(ctx, request.Login, hashPassword)
	if err != nil{
		fmt.Println("Error in service, while updating password!")
	}

	return &pb.AdminHashedPasswordResponse{
		HashedPassword: newPassword,
	}, nil
}







