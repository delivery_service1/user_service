package service

import (
	"context"
	"fmt"
	pb "user_service/genproto/user_protos"
	"user_service/grpc/client"
	"user_service/pkg"
	"user_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type clientService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedClientServiceServer
}

func NewClientService(strg storage.IStorage, services client.IServiceManager) *clientService {
	return &clientService{
		storage: strg,
		serivces: services,
	}
}

func (c *clientService) Create(ctx context.Context, request *pb.CreateClientRequest) (*pb.Client, error) {
	hashedPassword, err := pkg.HashPassword(request.GetPassword())
	if err != nil{
		fmt.Println("Error in service, while hashing admins password!",err.Error())
		return nil, err
	}

	request.Password = hashedPassword

	client, err := c.storage.Client().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating client!", err.Error())
		return nil, err
	}
	return client, nil
}

func (c *clientService) Get(ctx context.Context, request *pb.ClientPrimaryKey) (*pb.Client, error) {
	client, err := c.storage.Client().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting client!",err.Error())
		return nil, err
	}
	return client, nil
}

func (c *clientService) GetList(ctx context.Context, request *pb.GetClientListRequest) (*pb.ClientsResponse, error) {
	clients, err := c.storage.Client().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting clients list!", err.Error())
		return nil, err
	}
	return clients, nil
}

func (c *clientService) Update(ctx context.Context, request *pb.UpdateClient) (*pb.Client, error) {
	client, err := c.storage.Client().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating client!", err.Error())
		return nil, err
	}
	return client, nil
}

func (c *clientService) Delete(ctx context.Context, key *pb.ClientPrimaryKey) (*emptypb.Empty, error) {
	err := c.storage.Client().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting client!", err.Error())
		return nil,err
	}
	return nil,nil
}

func (c *clientService) UpdatePassword(ctx context.Context, request *pb.ChangeClientPasswordRequest) (*pb.ClientHashedPasswordResponse,error) {
	hashedPassword,err := c.storage.Client().GetPassword(ctx, request)
	if err != nil{
		fmt.Println("Error in service, login is wrong!",err.Error())
	}

	err = pkg.CheckPassword(ctx, hashedPassword, request.OldPassword)
	if err != nil{
		fmt.Println("Error in service, password doesn't match!", err.Error())
		return nil,err
	}
	
	hashPassword, err := pkg.HashPassword(request.NewPassword)
	if err != nil{
		fmt.Println("Error in service, while hashing password!", err.Error())
		return nil,err
	}

	newPassword, err := c.storage.Client().UpdatePassword(ctx, request.Login, hashPassword)
	if err != nil{
		fmt.Println("Error in service, while updating password!")
	}

	return &pb.ClientHashedPasswordResponse{
		HashedPassword: newPassword,
	}, nil
}

func (c *clientService) UpdateInfo(ctx context.Context, req *pb.UpdateClientInfo) (*emptypb.Empty, error) {
	err := c.storage.Client().UpdateInfo(ctx, req)
	fmt.Println("req:", req)
	if err != nil{
		fmt.Println("Error in service, while updating client info!",err.Error())
		return nil,err
	}
	return nil, nil
}

func (c *clientService) GetDiscountType(ctx context.Context, req *pb.ClientPrimaryKey) (*pb.ClientDiscountTypeResponse, error) {
	client, err := c.storage.Client().GetDiscountType(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting discount type of clients!", err.Error())
		return nil, err
	}
	return client, nil
}

