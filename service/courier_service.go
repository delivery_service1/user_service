package service

import (
	pb "user_service/genproto/user_protos"
	"user_service/grpc/client"
	"user_service/storage"
	"context"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
	"user_service/pkg"
)

type courierService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedCourierServiceServer
}

func NewCourierService(strg storage.IStorage, services client.IServiceManager) *courierService {
	return &courierService{
		storage: strg,
		serivces: services,
	}
}

func (c *courierService) Create(ctx context.Context, request *pb.CreateCourierRequest) (*pb.Courier, error) {
	hashedPassword, err := pkg.HashPassword(request.GetPassword())
	if err != nil{
		fmt.Println("Error in service, while hashing admins password!",err.Error())
		return nil, err
	}

	request.Password = hashedPassword

	courier, err := c.storage.Courier().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating courier!", err.Error())
		return nil, err
	}
	return courier, nil
}

func (c *courierService) Get(ctx context.Context, request *pb.CourierPrimaryKey) (*pb.Courier, error) {
	courier, err := c.storage.Courier().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting courier!",err.Error())
		return nil, err
	}
	return courier, nil
}

func (c *courierService) GetList(ctx context.Context, request *pb.GetCourierListRequest) (*pb.CouriersResponse, error) {
	couriers, err := c.storage.Courier().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, getting couriers list!", err.Error())
		return nil, err
	}
	return couriers, nil
}

func (c *courierService) Update(ctx context.Context, request *pb.UpdateCourier) (*pb.Courier, error) {
	courier, err := c.storage.Courier().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating courier!", err.Error())
		return nil, err
	}
	return courier, nil
}

func (c *courierService) Delete(ctx context.Context, key *pb.CourierPrimaryKey) (*emptypb.Empty, error) {
	err := c.storage.Courier().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting courier!", err.Error())
		return nil,err
	}
	return nil,nil
}

func (c *courierService) UpdatePassword(ctx context.Context, request *pb.ChangeCourierPasswordRequest) (*pb.CourierHashedPasswordResponse,error) {
	hashedPassword,err := c.storage.Courier().GetPassword(ctx, request)
	if err != nil{
		fmt.Println("Error in service, login is wrong!",err.Error())
	}

	fmt.Println("Old password: ",request.OldPassword)
	fmt.Println("hashed password: ",request.OldPassword)
	fmt.Println("new password: ",request.NewPassword)
	fmt.Println(" login: ",request.Login)

	err = pkg.CheckPassword(ctx, hashedPassword, request.OldPassword)
	if err != nil{
		fmt.Println("Error in service, password doesn't match!", err.Error())
		return nil,err
	}
	
	hashPassword, err := pkg.HashPassword(request.NewPassword)
	if err != nil{
		fmt.Println("Error in service, while hashing password!", err.Error())
		return nil,err
	}

	newPassword, err := c.storage.Courier().UpdatePassword(ctx, request.Login, hashPassword)
	if err != nil{
		fmt.Println("Error in service, while updating password!")
	}

	return &pb.CourierHashedPasswordResponse{
		HashedPassword: newPassword,
	}, nil
}


