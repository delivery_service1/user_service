package main

import (
	"user_service/config"
	"user_service/grpc"
	"user_service/grpc/client"
	"user_service/storage/postgres"
	"context"
	"fmt"
	"net"
)

func main(){
	cfg := config.Load()

	pgStore, err := postgres.New(context.Background(),cfg)
	if err != nil{
		fmt.Println("Error while connecting to database!",err.Error())
		return
	}

	defer pgStore.Close()

	services, err := client.NewGrpcClients(cfg)
	if err != nil {
		fmt.Println("Error while dialing grpc clients", err.Error())
		return
	}

	grpcServer := grpc.SetUpServer(pgStore, services)

	lis, err := net.Listen("tcp", cfg.ServiceGrpcHost+cfg.ServiceGrpcPort)
	if err != nil {
	fmt.Println("error while listening grpc host port", err.Error())
		return
	}

	fmt.Println("Service is running..., grpc port", cfg.ServiceGrpcPort)
	err = grpcServer.Serve(lis)
	if err != nil {
	fmt.Println("error while listening grpc", err.Error())
	}
}
