package client

import (
	"fmt"
	"user_service/config"
	order_service "user_service/genproto/order_protos"
	user_service "user_service/genproto/user_protos"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	AdminService()  user_service.AdminServiceClient
	BranchService() user_service.BranchServiceClient
	ClientService() user_service.ClientServiceClient
	CourierService() user_service.CourierServiceClient
	AuthService() user_service.AuthServiceClient

	// order service
	DeliveryTarif() order_service.DeliveryTarifServiceClient
}

type grpcClients struct {
	adminService user_service.AdminServiceClient
	branchService user_service.BranchServiceClient
	clientService user_service.ClientServiceClient
	courierService user_service.CourierServiceClient

	authService user_service.AuthServiceClient

	// order service

	deliveryTarifService order_service.DeliveryTarifServiceClient
}

func NewGrpcClients (cfg config.Config)(IServiceManager, error){
	connUserService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting to api_gateway!",err.Error())
		return nil, err
	}

	connOrderService, err := grpc.Dial(
		cfg.OrderServiceGrpcHost+cfg.OrderServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting to api_gateway!",err.Error())
		return nil, err
	}

	return &grpcClients{
		adminService: user_service.NewAdminServiceClient(connUserService),
		branchService: user_service.NewBranchServiceClient(connUserService),
		clientService: user_service.NewClientServiceClient(connUserService),
		courierService: user_service.NewCourierServiceClient(connUserService),

		authService: user_service.NewAuthServiceClient(connUserService),

		// order service 

		deliveryTarifService: order_service.NewDeliveryTarifServiceClient(connOrderService),

	}, nil
}

func (g *grpcClients) AdminService() user_service.AdminServiceClient{
	return g.adminService
}

func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) CourierService() user_service.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) AuthService() user_service.AuthServiceClient {
	return g.authService
}

func (g *grpcClients) DeliveryTarif() order_service.DeliveryTarifServiceClient {
	return g.deliveryTarifService
}
 