package grpc

import (
	pb "user_service/genproto/user_protos"
	"user_service/grpc/client"
	"user_service/service"
	"user_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer (strg storage.IStorage, services client.IServiceManager)*grpc.Server{
	grpcServer := grpc.NewServer()
	
	pb.RegisterAdminServiceServer(grpcServer, service.NewAdminService(strg, services))
	pb.RegisterBranchServiceServer(grpcServer, service.NewBranchService(strg, services))
	pb.RegisterClientServiceServer(grpcServer, service.NewClientService(strg, services))
	pb.RegisterCourierServiceServer(grpcServer, service.NewCourierService(strg, services))
	pb.RegisterAuthServiceServer(grpcServer,service.NewAuthService(strg,services))

	reflection.Register(grpcServer)

	return grpcServer
}