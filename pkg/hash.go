package pkg

import (
	"context"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

func CheckPassword (ctx context.Context, hashedPassword,oldPassword string) (error){
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(oldPassword))
	if err != nil{
		fmt.Println("Error, password don't match!", err.Error())
		return err
	}
	return nil
}

func HashPassword (password string) (string, error){
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password),bcrypt.DefaultCost)
	if err != nil{
		fmt.Println("Error while hashing password!", err.Error())
		return "", err
	}

	return string(hashedPassword), nil
}