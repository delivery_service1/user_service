// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.33.0
// 	protoc        v3.12.4
// source: order_protos/delivery_tarif_alternative_service.proto

package order_service

import (
	empty "github.com/golang/protobuf/ptypes/empty"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CreateDeliveryTarifAlternativeRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Type string `protobuf:"bytes,1,opt,name=type,proto3" json:"type,omitempty"`
}

func (x *CreateDeliveryTarifAlternativeRequest) Reset() {
	*x = CreateDeliveryTarifAlternativeRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateDeliveryTarifAlternativeRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateDeliveryTarifAlternativeRequest) ProtoMessage() {}

func (x *CreateDeliveryTarifAlternativeRequest) ProtoReflect() protoreflect.Message {
	mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateDeliveryTarifAlternativeRequest.ProtoReflect.Descriptor instead.
func (*CreateDeliveryTarifAlternativeRequest) Descriptor() ([]byte, []int) {
	return file_order_protos_delivery_tarif_alternative_service_proto_rawDescGZIP(), []int{0}
}

func (x *CreateDeliveryTarifAlternativeRequest) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

type DeliveryTarifAlternative struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Type      string `protobuf:"bytes,2,opt,name=type,proto3" json:"type,omitempty"`
	CreatedAt string `protobuf:"bytes,3,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt string `protobuf:"bytes,4,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *DeliveryTarifAlternative) Reset() {
	*x = DeliveryTarifAlternative{}
	if protoimpl.UnsafeEnabled {
		mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeliveryTarifAlternative) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeliveryTarifAlternative) ProtoMessage() {}

func (x *DeliveryTarifAlternative) ProtoReflect() protoreflect.Message {
	mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeliveryTarifAlternative.ProtoReflect.Descriptor instead.
func (*DeliveryTarifAlternative) Descriptor() ([]byte, []int) {
	return file_order_protos_delivery_tarif_alternative_service_proto_rawDescGZIP(), []int{1}
}

func (x *DeliveryTarifAlternative) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *DeliveryTarifAlternative) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *DeliveryTarifAlternative) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *DeliveryTarifAlternative) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type DeliveryTarifAlternativePrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *DeliveryTarifAlternativePrimaryKey) Reset() {
	*x = DeliveryTarifAlternativePrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeliveryTarifAlternativePrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeliveryTarifAlternativePrimaryKey) ProtoMessage() {}

func (x *DeliveryTarifAlternativePrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeliveryTarifAlternativePrimaryKey.ProtoReflect.Descriptor instead.
func (*DeliveryTarifAlternativePrimaryKey) Descriptor() ([]byte, []int) {
	return file_order_protos_delivery_tarif_alternative_service_proto_rawDescGZIP(), []int{2}
}

func (x *DeliveryTarifAlternativePrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type UpdateDeliveryTarifAlternative struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id   string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Type string `protobuf:"bytes,2,opt,name=type,proto3" json:"type,omitempty"`
}

func (x *UpdateDeliveryTarifAlternative) Reset() {
	*x = UpdateDeliveryTarifAlternative{}
	if protoimpl.UnsafeEnabled {
		mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateDeliveryTarifAlternative) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateDeliveryTarifAlternative) ProtoMessage() {}

func (x *UpdateDeliveryTarifAlternative) ProtoReflect() protoreflect.Message {
	mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateDeliveryTarifAlternative.ProtoReflect.Descriptor instead.
func (*UpdateDeliveryTarifAlternative) Descriptor() ([]byte, []int) {
	return file_order_protos_delivery_tarif_alternative_service_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateDeliveryTarifAlternative) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateDeliveryTarifAlternative) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

type GetDeliveryTarifAlternativeListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page   int32  `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
	Limit  int32  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetDeliveryTarifAlternativeListRequest) Reset() {
	*x = GetDeliveryTarifAlternativeListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetDeliveryTarifAlternativeListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetDeliveryTarifAlternativeListRequest) ProtoMessage() {}

func (x *GetDeliveryTarifAlternativeListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetDeliveryTarifAlternativeListRequest.ProtoReflect.Descriptor instead.
func (*GetDeliveryTarifAlternativeListRequest) Descriptor() ([]byte, []int) {
	return file_order_protos_delivery_tarif_alternative_service_proto_rawDescGZIP(), []int{4}
}

func (x *GetDeliveryTarifAlternativeListRequest) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *GetDeliveryTarifAlternativeListRequest) GetLimit() int32 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetDeliveryTarifAlternativeListRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type DeliveryTarifAlternativesResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	DeliveryTarifAlternatives []*DeliveryTarifAlternative `protobuf:"bytes,1,rep,name=deliveryTarifAlternatives,proto3" json:"deliveryTarifAlternatives,omitempty"`
	Count                     int32                       `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
}

func (x *DeliveryTarifAlternativesResponse) Reset() {
	*x = DeliveryTarifAlternativesResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeliveryTarifAlternativesResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeliveryTarifAlternativesResponse) ProtoMessage() {}

func (x *DeliveryTarifAlternativesResponse) ProtoReflect() protoreflect.Message {
	mi := &file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeliveryTarifAlternativesResponse.ProtoReflect.Descriptor instead.
func (*DeliveryTarifAlternativesResponse) Descriptor() ([]byte, []int) {
	return file_order_protos_delivery_tarif_alternative_service_proto_rawDescGZIP(), []int{5}
}

func (x *DeliveryTarifAlternativesResponse) GetDeliveryTarifAlternatives() []*DeliveryTarifAlternative {
	if x != nil {
		return x.DeliveryTarifAlternatives
	}
	return nil
}

func (x *DeliveryTarifAlternativesResponse) GetCount() int32 {
	if x != nil {
		return x.Count
	}
	return 0
}

var File_order_protos_delivery_tarif_alternative_service_proto protoreflect.FileDescriptor

var file_order_protos_delivery_tarif_alternative_service_proto_rawDesc = []byte{
	0x0a, 0x35, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x73, 0x2f, 0x64,
	0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x5f, 0x74, 0x61, 0x72, 0x69, 0x66, 0x5f, 0x61, 0x6c,
	0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x22, 0x3b, 0x0a, 0x25, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6c,
	0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e,
	0x61, 0x74, 0x69, 0x76, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04,
	0x74, 0x79, 0x70, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65,
	0x22, 0x7c, 0x0a, 0x18, 0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69,
	0x66, 0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04,
	0x74, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65,
	0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12,
	0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x04, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x34,
	0x0a, 0x22, 0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41,
	0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72,
	0x79, 0x4b, 0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x02, 0x69, 0x64, 0x22, 0x44, 0x0a, 0x1e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x44, 0x65,
	0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72,
	0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x22, 0x6a, 0x0a, 0x26, 0x47, 0x65,
	0x74, 0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c,
	0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x04, 0x70, 0x61, 0x67, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16,
	0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06,
	0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0xa0, 0x01, 0x0a, 0x21, 0x44, 0x65, 0x6c, 0x69, 0x76,
	0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x74,
	0x69, 0x76, 0x65, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x65, 0x0a, 0x19,
	0x64, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74,
	0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x27, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74,
	0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x52, 0x19, 0x64, 0x65, 0x6c, 0x69, 0x76, 0x65,
	0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69,
	0x76, 0x65, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x32, 0xa2, 0x04, 0x0a, 0x1f, 0x44, 0x65,
	0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72,
	0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x69, 0x0a,
	0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x34, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x44, 0x65,
	0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72,
	0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x27, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x44, 0x65,
	0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72,
	0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x22, 0x00, 0x12, 0x63, 0x0a, 0x03, 0x47, 0x65, 0x74, 0x12,
	0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74,
	0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b,
	0x65, 0x79, 0x1a, 0x27, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66,
	0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x22, 0x00, 0x12, 0x74, 0x0a,
	0x07, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x35, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x44, 0x65, 0x6c, 0x69,
	0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61,
	0x74, 0x69, 0x76, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x30, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74,
	0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x22, 0x00, 0x12, 0x62, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x2d, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69,
	0x66, 0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x1a, 0x27, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x44, 0x65, 0x6c,
	0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41, 0x6c, 0x74, 0x65, 0x72, 0x6e,
	0x61, 0x74, 0x69, 0x76, 0x65, 0x22, 0x00, 0x12, 0x55, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x12, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x54, 0x61, 0x72, 0x69, 0x66, 0x41,
	0x6c, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x74, 0x69, 0x76, 0x65, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72,
	0x79, 0x4b, 0x65, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x18,
	0x5a, 0x16, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_order_protos_delivery_tarif_alternative_service_proto_rawDescOnce sync.Once
	file_order_protos_delivery_tarif_alternative_service_proto_rawDescData = file_order_protos_delivery_tarif_alternative_service_proto_rawDesc
)

func file_order_protos_delivery_tarif_alternative_service_proto_rawDescGZIP() []byte {
	file_order_protos_delivery_tarif_alternative_service_proto_rawDescOnce.Do(func() {
		file_order_protos_delivery_tarif_alternative_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_order_protos_delivery_tarif_alternative_service_proto_rawDescData)
	})
	return file_order_protos_delivery_tarif_alternative_service_proto_rawDescData
}

var file_order_protos_delivery_tarif_alternative_service_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_order_protos_delivery_tarif_alternative_service_proto_goTypes = []interface{}{
	(*CreateDeliveryTarifAlternativeRequest)(nil),  // 0: order_service.CreateDeliveryTarifAlternativeRequest
	(*DeliveryTarifAlternative)(nil),               // 1: order_service.DeliveryTarifAlternative
	(*DeliveryTarifAlternativePrimaryKey)(nil),     // 2: order_service.DeliveryTarifAlternativePrimaryKey
	(*UpdateDeliveryTarifAlternative)(nil),         // 3: order_service.UpdateDeliveryTarifAlternative
	(*GetDeliveryTarifAlternativeListRequest)(nil), // 4: order_service.GetDeliveryTarifAlternativeListRequest
	(*DeliveryTarifAlternativesResponse)(nil),      // 5: order_service.DeliveryTarifAlternativesResponse
	(*empty.Empty)(nil),                            // 6: google.protobuf.Empty
}
var file_order_protos_delivery_tarif_alternative_service_proto_depIdxs = []int32{
	1, // 0: order_service.DeliveryTarifAlternativesResponse.deliveryTarifAlternatives:type_name -> order_service.DeliveryTarifAlternative
	0, // 1: order_service.DeliveryTarifAlternativeService.Create:input_type -> order_service.CreateDeliveryTarifAlternativeRequest
	2, // 2: order_service.DeliveryTarifAlternativeService.Get:input_type -> order_service.DeliveryTarifAlternativePrimaryKey
	4, // 3: order_service.DeliveryTarifAlternativeService.GetList:input_type -> order_service.GetDeliveryTarifAlternativeListRequest
	3, // 4: order_service.DeliveryTarifAlternativeService.Update:input_type -> order_service.UpdateDeliveryTarifAlternative
	2, // 5: order_service.DeliveryTarifAlternativeService.Delete:input_type -> order_service.DeliveryTarifAlternativePrimaryKey
	1, // 6: order_service.DeliveryTarifAlternativeService.Create:output_type -> order_service.DeliveryTarifAlternative
	1, // 7: order_service.DeliveryTarifAlternativeService.Get:output_type -> order_service.DeliveryTarifAlternative
	5, // 8: order_service.DeliveryTarifAlternativeService.GetList:output_type -> order_service.DeliveryTarifAlternativesResponse
	1, // 9: order_service.DeliveryTarifAlternativeService.Update:output_type -> order_service.DeliveryTarifAlternative
	6, // 10: order_service.DeliveryTarifAlternativeService.Delete:output_type -> google.protobuf.Empty
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_order_protos_delivery_tarif_alternative_service_proto_init() }
func file_order_protos_delivery_tarif_alternative_service_proto_init() {
	if File_order_protos_delivery_tarif_alternative_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateDeliveryTarifAlternativeRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeliveryTarifAlternative); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeliveryTarifAlternativePrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateDeliveryTarifAlternative); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetDeliveryTarifAlternativeListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_order_protos_delivery_tarif_alternative_service_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeliveryTarifAlternativesResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_order_protos_delivery_tarif_alternative_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_order_protos_delivery_tarif_alternative_service_proto_goTypes,
		DependencyIndexes: file_order_protos_delivery_tarif_alternative_service_proto_depIdxs,
		MessageInfos:      file_order_protos_delivery_tarif_alternative_service_proto_msgTypes,
	}.Build()
	File_order_protos_delivery_tarif_alternative_service_proto = out.File
	file_order_protos_delivery_tarif_alternative_service_proto_rawDesc = nil
	file_order_protos_delivery_tarif_alternative_service_proto_goTypes = nil
	file_order_protos_delivery_tarif_alternative_service_proto_depIdxs = nil
}
