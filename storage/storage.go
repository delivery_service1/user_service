package storage

import (
	"context"
	pb "user_service/genproto/user_protos"
)

type IStorage interface {
	Close()
	Admin() IAdminStorage
	Branch() IBranchStorage
	Client() IClientStorage
	Courier() ICourierStorage
}

type IAdminStorage interface {
	Create(context.Context, *pb.CreateAdminRequest) (*pb.Admin, error)
	Get(context.Context, *pb.AdminPrimaryKey) (*pb.Admin, error)
	GetList(context.Context, *pb.GetAdminListRequest) (*pb.AdminsResponse, error)
	Update(context.Context, *pb.UpdateAdmin) (*pb.Admin, error)
	Delete(context.Context, *pb.AdminPrimaryKey) (error)
	GetAdminByCredentials(context.Context,*pb.UserLoginRequest)(*pb.Admin, error)
	GetPassword(context.Context, *pb.ChangeAdminPasswordRequest) (string,error)
	UpdatePassword(context.Context, string, string) (string, error)
}

type IBranchStorage interface {
	Create(context.Context, *pb.CreateBranchRequest) (*pb.Branch, error)
	Get(context.Context, *pb.BranchPrimaryKey) (*pb.Branch, error)
	GetList(context.Context, *pb.GetBranchListRequest) (*pb.BranchesResponse, error)
	Update(context.Context, *pb.UpdateBranch) (*pb.Branch, error)
	Delete(context.Context, *pb.BranchPrimaryKey) (error)

	GetActiveBranchesList(context.Context,*pb.GetBranchListRequest)(*pb.BranchesResponse, error)
}

type IClientStorage interface {
	Create(context.Context, *pb.CreateClientRequest) (*pb.Client, error)
	Get(context.Context, *pb.ClientPrimaryKey) (*pb.Client, error)
	GetList(context.Context, *pb.GetClientListRequest) (*pb.ClientsResponse, error)
	Update(context.Context, *pb.UpdateClient) (*pb.Client, error)
	Delete(context.Context, *pb.ClientPrimaryKey) (error)
	GetClientByCredentials(context.Context,*pb.UserLoginRequest)(*pb.Client, error)
	GetPassword(context.Context, *pb.ChangeClientPasswordRequest) (string,error)
	UpdatePassword(context.Context, string, string) (string, error)
	UpdateInfo(context.Context,*pb.UpdateClientInfo)(error)
	GetDiscountType(context.Context,*pb.ClientPrimaryKey)(*pb.ClientDiscountTypeResponse,error)
}
 
type ICourierStorage interface {
	Create(context.Context, *pb.CreateCourierRequest) (*pb.Courier, error)
	Get(context.Context, *pb.CourierPrimaryKey) (*pb.Courier, error)
	GetList(context.Context, *pb.GetCourierListRequest) (*pb.CouriersResponse, error)
	Update(context.Context, *pb.UpdateCourier) (*pb.Courier, error)
	Delete(context.Context, *pb.CourierPrimaryKey) (error)
	GetCourierByCredentials(context.Context,*pb.UserLoginRequest)(*pb.Courier, error)
	GetPassword(context.Context, *pb.ChangeCourierPasswordRequest) (string,error)
	UpdatePassword(context.Context, string, string) (string, error)
}

