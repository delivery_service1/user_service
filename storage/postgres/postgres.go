package postgres

import (
	"user_service/config"
	"user_service/storage"
	"context"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Store struct {
	db *pgxpool.Pool
	cfg config.Config
}

func New (ctx context.Context, cfg config.Config)(storage.IStorage, error){

	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)


	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil{
		fmt.Println("Error while parsing config!",err.Error())
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil{
		fmt.Println("Error while getting new pool!", err.Error())
		return Store{},err
	}
	
	return Store{
		db: pool,
		cfg: cfg,
	}, nil
}

func (s Store) Close(){
	s.db.Close()
}

func (s Store) Admin() storage.IAdminStorage{
	return NewAdminRepo(s.db)
}

func (s Store) Branch() storage.IBranchStorage {
	return NewBranchRepo(s.db)
}

func (s Store) Client() storage.IClientStorage {
	return NewClientRepo(s.db)
}

func (s Store) Courier() storage.ICourierStorage {
	return NewCourierRepo(s.db)
}