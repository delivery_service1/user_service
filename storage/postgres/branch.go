package postgres

import (
	"context"
	"fmt"
	"time"
	pb "user_service/genproto/user_protos"
	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type branchRepo struct {
	DB *pgxpool.Pool
}

func NewBranchRepo (db *pgxpool.Pool) storage.IBranchStorage{
	return &branchRepo{
		DB: db,
	}
}

func (b *branchRepo) Create(ctx context.Context, createBranch *pb.CreateBranchRequest) (*pb.Branch, error) {
	var (
		id = uuid.New()
		branch = pb.Branch{}
	)
	
	query := `
		insert into branches (id, name, phone, delivery_tarif_id, work_hour_start, work_hour_end,
			address, destination, active)
			values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
			returning id, name, phone, delivery_tarif_id, work_hour_start, work_hour_end,
				address, destination, active::text, created_at::text `

	err := b.DB.QueryRow(ctx, query,
		 	id, 
			createBranch.GetName(),
			createBranch.GetPhone(),
			createBranch.GetDeliveryTarifId(),
			createBranch.GetWorkHourStart(),
			createBranch.GetWorkHourEnd(),
			createBranch.GetAddress(),
			createBranch.GetDestination(),
			createBranch.GetActive(),
		).Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTarifId,
			&branch.WorkHourStart,
			&branch.WorkHourEnd,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating Branch!", err.Error())
			return nil, err
	 	} 

		return &branch, nil
}

func (b *branchRepo) Get(ctx context.Context, key *pb.BranchPrimaryKey) (*pb.Branch, error) {
	var (
		branch = pb.Branch{}
	)

	query := `select id, name, phone, delivery_tarif_id, work_hour_start, work_hour_end,
	address, destination, active::text, created_at::text, updated_at::text from branches
		where deleted_at = 0 and id = $1 `

	err := b.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.DeliveryTarifId,
		&branch.WorkHourStart,
		&branch.WorkHourEnd,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
		&branch.CreatedAt,
		&branch.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Branch by id!", err.Error())
		return nil, err
	}

	return &branch, nil
}

func (b *branchRepo) GetList(ctx context.Context, request *pb.GetBranchListRequest) (*pb.BranchesResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		branches = pb.BranchesResponse{}
		filter, countQuery string
		search = request.Search
	)
	// search: name

	countQuery = `select count(1) from branches WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND name ilike '%%%s%%' `, search)
	
	if search != ""{
		countQuery += filter
	}

	err := b.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of Branchs",err.Error())
		return nil, err
	}

	query := `select id, name, phone, delivery_tarif_id, work_hour_start, work_hour_end,
	address, destination, active::text, created_at::text, updated_at::text from branches
		where deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := b.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting Branchs!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			branch = pb.Branch{}
		)

		err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTarifId,
			&branch.WorkHourStart,
			&branch.WorkHourEnd,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
			&branch.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning Branchs!",err.Error())
			return nil, err
		}
		branches.Branches = append(branches.Branches, &branch)
	}

	branches.Count = count

	return &branches, nil
}

func (b *branchRepo) Update(ctx context.Context, updBranch *pb.UpdateBranch) (*pb.Branch, error) {
	branch := pb.Branch{}

	query := `UPDATE branches SET name = $1, phone = $2, delivery_tarif_id = $3, work_hour_start = $4,
			work_hour_end = $5, address = $6, destination = $7, active = $8, updated_at = now()
		WHERE id = $9 AND deleted_at = 0 
	returning id, name, phone, delivery_tarif_id, work_hour_start, work_hour_end, address, 
destination, active::text, updated_at::text `

	err := b.DB.QueryRow(ctx, query,
		updBranch.GetName(),
		updBranch.GetPhone(),
		updBranch.GetDeliveryTarifId(),
		updBranch.GetWorkHourStart(),
		updBranch.GetWorkHourEnd(),
		updBranch.GetAddress(),
		updBranch.GetDestination(),
		updBranch.GetActive(),
		updBranch.GetId(),
	).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.DeliveryTarifId,
		&branch.WorkHourStart,
		&branch.WorkHourEnd,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
		&branch.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating Branch!", err.Error())
		return nil, err
	}

	return &branch, nil
}

func (b *branchRepo) Delete(ctx context.Context, key *pb.BranchPrimaryKey) (error) {
	query := `UPDATE branches set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := b.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting Branch by id!", err.Error())
		return err
	}
	return nil
}

func (b *branchRepo) GetActiveBranchesList(ctx context.Context, request *pb.GetBranchListRequest) (*pb.BranchesResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		branches = pb.BranchesResponse{}
		countQuery string
	)

	currentTime := time.Now().Format("15:04")

	countQuery = `select count(1) from branches WHERE active = 'yes'
	AND work_hour_start <= $1 AND work_hour_end >= $2 ` 

 
	err := b.DB.QueryRow(ctx, countQuery, currentTime, currentTime).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of Branches",err.Error())
		return nil, err
	}

	query := `
	SELECT id, name, phone, delivery_tarif_id, work_hour_start, work_hour_end, address, destination, active::text, created_at::text, updated_at::text
			FROM branches WHERE active = 'yes'
	AND work_hour_start <= $1 AND work_hour_end >= $2
`
	query += ` OFFSET $3 LIMIT $4`

	rows, err := b.DB.Query(ctx, query, currentTime, currentTime,offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting Branches!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			branch = pb.Branch{}
		)

		err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTarifId,
			&branch.WorkHourStart,
			&branch.WorkHourEnd,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
			&branch.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning Branchs!",err.Error())
			return nil, err
		}
		branches.Branches = append(branches.Branches, &branch)
	}

	branches.Count = count

	return &branches, nil
}