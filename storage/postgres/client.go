package postgres

import (
	"context"
	"fmt"
	pb "user_service/genproto/user_protos"
	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type clientRepo struct {
	DB *pgxpool.Pool
}

func NewClientRepo (db *pgxpool.Pool) storage.IClientStorage{
	return &clientRepo{
		DB: db,
	}
}

func (c *clientRepo) Create(ctx context.Context, createClient *pb.CreateClientRequest) (*pb.Client, error) {
	var (
		id = uuid.New()
		client = pb.Client{}
	)

	query := `
		insert into clients (id, first_name, last_name, phone, login, password, date_of_birth,
			last_ordered_date, total_orders_sum, total_orders_count, discount_type, discount_amount)
				values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
		returning id, first_name, last_name, phone, login, password, date_of_birth::text,
	last_ordered_date::text, total_orders_sum, total_orders_count,
 discount_type::text, discount_amount, created_at::text `

	err := c.DB.QueryRow(ctx, query,
		 	id, 
			createClient.GetFirstName(), 
			createClient.GetLastName(),
			createClient.GetPhone(),
 			createClient.GetLogin(),
			createClient.GetPassword(),
			createClient.GetDateOfBirth(),
			createClient.GetLastOrderedDate(),
			createClient.GetTotalOrdersSum(),
			createClient.GetTotalOrdersCount(),
			createClient.GetDiscountType(),
			createClient.GetDiscountAmount(),
		).Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&client.Login,
			&client.Password,
			&client.DateOfBirth,
			&client.LastOrderedDate,
			&client.TotalOrdersSum,
			&client.TotalOrdersCount,
			&client.DiscountType,
			&client.DiscountAmount,
			&client.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating Client!", err.Error())
			return nil, err
	 	} 

		return &client, nil
}

func (c *clientRepo) Get(ctx context.Context, key *pb.ClientPrimaryKey) (*pb.Client, error) {
	var (
		client = pb.Client{}
	)

	query := `select id, first_name, last_name, phone, login, date_of_birth::text,
	last_ordered_date::text, total_orders_sum, total_orders_count,
 discount_type::text, discount_amount, created_at::text, updated_at::text from clients
		where deleted_at = 0 and id = $1 `

	err := c.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Login,
 		&client.DateOfBirth,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
		&client.CreatedAt,
		&client.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Client by id!", err.Error())
		return nil, err
	}

	return &client, nil
}

func (c *clientRepo) GetList(ctx context.Context, request *pb.GetClientListRequest) (*pb.ClientsResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		clients = pb.ClientsResponse{}
		filter, countQuery string
		search = request.Search
	)
	// search: firstname, lastname, phone

	countQuery = `select count(1) from clients WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND first_name ilike '%%%s%%' OR last_name ilike '%%%s%%' OR phone ilike '%%%s%%' `,search, search, search)
	
	if search != ""{
		countQuery += filter
	}

	err := c.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of Clients",err.Error())
		return nil, err
	}

	query := `select id, first_name, last_name, phone, login, date_of_birth::text,
	last_ordered_date::text, total_orders_sum, total_orders_count,
 discount_type::text, discount_amount, created_at::text, updated_at::text from clients
		where deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := c.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting Clients!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			client = pb.Client{}
		)

		err = rows.Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&client.Login,
			&client.DateOfBirth,
			&client.LastOrderedDate,
			&client.TotalOrdersSum,
			&client.TotalOrdersCount,
			&client.DiscountType,
			&client.DiscountAmount,
			&client.CreatedAt,
			&client.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning Clients!",err.Error())
			return nil, err
		}
		clients.Clients = append(clients.Clients, &client)
	}

	clients.Count = count

	return &clients, nil
}

func (c *clientRepo) Update(ctx context.Context, updClient *pb.UpdateClient) (*pb.Client, error) {
	client := pb.Client{}

	query :=   `UPDATE clients SET first_name = $1, last_name = $2, phone = $3,  
	date_of_birth = $4, last_ordered_date = $5, total_orders_sum = $6,
		total_orders_count = $7, discount_type = $8, discount_amount = $9,  updated_at = now() 
				WHERE id = $10 AND deleted_at = 0
	returning id, first_name, last_name, phone, date_of_birth::text,
		last_ordered_date::text, total_orders_sum, total_orders_count,
			discount_type::text, discount_amount, updated_at::text`

	err := c.DB.QueryRow(ctx, query,
		updClient.GetFirstName(),
		updClient.GetLastName(),
		updClient.GetPhone(),
		updClient.GetDateOfBirth(),
		updClient.GetLastOrderedDate(),
		updClient.GetTotalOrdersSum(),
		updClient.GetTotalOrdersCount(),
		updClient.GetDiscountType(),
		updClient.GetDiscountAmount(),
		updClient.GetId(),
	).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.DateOfBirth,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
 		&client.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating Clients!", err.Error())
		return nil, err
	}

	return &client, nil
}

func (c *clientRepo) Delete(ctx context.Context, key *pb.ClientPrimaryKey) (error) {
	query := `UPDATE clients set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := c.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting Client by id!", err.Error())
		return err
	}
	return nil
}

// Change password!
func (c *clientRepo) GetPassword(ctx context.Context, request *pb.ChangeClientPasswordRequest) (string,error) {
	query := `SELECT password from clients 
		WHERE deleted_at = 0 AND login = $1 `
	
	var oldPassword string
	
	err := c.DB.QueryRow(ctx, query, request.Login).Scan(
		&oldPassword,
	)
	if err != nil{
		fmt.Println("Login is wrong !", err.Error())
		return "",err
	}

	return oldPassword,nil
}

func (c *clientRepo) UpdatePassword(ctx context.Context, login, password string) (string, error) {
	query := `UPDATE clients SET password = $1 
			WHERE deleted_at = 0 AND login = $2
		returning password `
	

	err := c.DB.QueryRow(ctx, query, password, login).Scan(
		&password,
	)
	if err != nil{
		fmt.Println("Error while updating couriers password!", err.Error())
		return "", err
	}

	return password, nil
}


func (c *clientRepo) GetClientByCredentials(ctx context.Context, req *pb.UserLoginRequest) (*pb.Client, error) {
	client := pb.Client{}

	if err := c.DB.QueryRow(ctx, `select id, phone, login, password from clients where login = $1`, req.GetLogin()).Scan(
		&client.Id,
		&client.Phone,
		&client.Login,
		&client.Password,
	); err != nil {
		fmt.Println("Error while getting client credentials!",err.Error())
		return nil, err
	}

	return &client, nil
}

func (c *clientRepo) UpdateInfo(ctx context.Context, req *pb.UpdateClientInfo) (error) {
	query := `UPDATE clients SET last_ordered_date = now(), total_orders_sum = total_orders_sum + $1,
			total_orders_count = total_orders_count + $2, discount_amount = discount_amount + $3 WHERE id = $4 `
	_, err := c.DB.Exec(ctx,query,
 		req.GetTotalOrdersSum(), 
		req.GetTotalOrdersCount(), 
		req.GetDiscountAmount(),
		req.GetId(), 
	)
	if err != nil{
		fmt.Println("Error while updating clients info!", err.Error())
		return err
	}
	return nil
}

func (c *clientRepo) GetDiscountType(ctx context.Context, req *pb.ClientPrimaryKey) (*pb.ClientDiscountTypeResponse,error) {
	client := pb.ClientDiscountTypeResponse{}
	query := `SELECT discount_type from clients where id = $1 `

	err := c.DB.QueryRow(ctx, query,req.GetId()).Scan(
		&client.DiscountType,
	)
	if err != nil{
		fmt.Println("Error while selecting discount type of client!", err.Error())
		return nil, err
	}

	return &client, nil
}