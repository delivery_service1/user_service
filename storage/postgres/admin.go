package postgres

import (
	"context"
	"fmt"
	pb "user_service/genproto/user_protos"
	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type adminRepo struct {
	DB *pgxpool.Pool
}

func NewAdminRepo (db *pgxpool.Pool) storage.IAdminStorage{
	return &adminRepo{
		DB: db,
	}
}

func (a *adminRepo) Create(ctx context.Context, createAdmin *pb.CreateAdminRequest) (*pb.Admin, error) {
	var (
		id = uuid.New()
		admin = pb.Admin{}
	)
 
	query := `
		insert into admins (id, first_name, last_name, phone, active, login, password)
			values ($1, $2, $3, $4, $5, $6, $7)
			returning id, first_name, last_name, phone, active::text, login, 
			created_at::text `

	err := a.DB.QueryRow(ctx, query,
		 	id, 
			createAdmin.GetFirstName(), 
			createAdmin.GetLastName(),
			createAdmin.GetPhone(),
			createAdmin.GetActive(),
			createAdmin.GetLogin(),
			createAdmin.GetPassword(),
		).Scan(
			&admin.Id,
			&admin.FirstName,
			&admin.LastName,
			&admin.Phone,
			&admin.Active,
			&admin.Login,
			&admin.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating Admin!", err.Error())
			return nil, err
	 	} 

		return &admin, nil
}

func (a *adminRepo) Get(ctx context.Context, key *pb.AdminPrimaryKey) (*pb.Admin, error) {
	var (
		admin = pb.Admin{}
	)

	query := `select id, first_name, last_name, phone, active::text,
	login, created_at::text, updated_at::text from admins
		where deleted_at = 0 and id = $1 `

	err := a.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&admin.Id,
		&admin.FirstName,
		&admin.LastName,
		&admin.Phone,
		&admin.Active,
		&admin.Login,
 		&admin.CreatedAt,
		&admin.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Admin by id!", err.Error())
		return nil, err
	}

	return &admin, nil
}

func (a *adminRepo) GetList(ctx context.Context, request *pb.GetAdminListRequest) (*pb.AdminsResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		admins = pb.AdminsResponse{}
		filter, countQuery string
		search = request.Search
	)
	// search: firstname, lastname, phone

	countQuery = `select count(1) from admins WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND first_name ilike '%%%s%%' OR last_name ilike '%%%s%%' OR phone ilike '%%%s%%' `,search, search, search)
	
	if search != ""{
		countQuery += filter
	}

	err := a.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of admins",err.Error())
		return nil, err
	}

	query := ` select id, first_name, last_name, phone, active::text,
	login, created_at::text, updated_at::text from admins
		where deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := a.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting admins!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			admin = pb.Admin{}
		)

		err = rows.Scan(
			&admin.Id,
			&admin.FirstName,
			&admin.LastName,
			&admin.Phone,
			&admin.Active,
			&admin.Login,
 			&admin.CreatedAt,
			&admin.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning admins!",err.Error())
			return nil, err
		}
		admins.Admins = append(admins.Admins, &admin)
	}

	admins.Count = count

	return &admins, nil
}

func (a *adminRepo) Update(ctx context.Context, updAdmin *pb.UpdateAdmin) (*pb.Admin, error) {
	admin := pb.Admin{}

	query :=  `UPDATE admins SET first_name = $1, last_name = $2, phone = $3, active = $4,
	updated_at = now() WHERE id = $5 and deleted_at = 0 
		returning id, first_name, last_name, phone, active::text, updated_at::text `

	err := a.DB.QueryRow(ctx, query,
		updAdmin.GetFirstName(),
		updAdmin.GetLastName(),
		updAdmin.GetPhone(),
		updAdmin.GetActive(),
		updAdmin.GetId(),
	).Scan(
		&admin.Id,
		&admin.FirstName,
		&admin.LastName,
		&admin.Phone,
		&admin.Active,
		&admin.CreatedAt,
	)

	if err != nil{
		fmt.Println("error while updating admins!", err.Error())
		return nil, err
	}

	return &admin, nil
}

func (c *adminRepo) Delete(ctx context.Context, key *pb.AdminPrimaryKey) (error) {
	query := `UPDATE admins set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := c.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting Admin by id!", err.Error())
		return err
	}
	return nil
}


// Change password!
func (a *adminRepo) GetPassword(ctx context.Context, request *pb.ChangeAdminPasswordRequest) (string,error) {
	query := `SELECT password from admins 
		WHERE deleted_at = 0 AND login = $1 `
	
	var oldPassword string
	
	err := a.DB.QueryRow(ctx, query, request.Login).Scan(
		&oldPassword,
	)
	if err != nil{
		fmt.Println("Login is wrong !", err.Error())
		return "",err
	}

	return oldPassword,nil
}

func (a *adminRepo) UpdatePassword(ctx context.Context, login, password string) (string, error) {
	query := `UPDATE admins SET password = $1 
			WHERE deleted_at = 0 AND login = $2
		returning password `
	

	err := a.DB.QueryRow(ctx, query, password, login).Scan(
		&password,
	)
	if err != nil{
		fmt.Println("Error while updating couriers password!", err.Error())
		return "", err
	}

	return password, nil
}

func (a *adminRepo) GetAdminByCredentials(ctx context.Context, req *pb.UserLoginRequest) (*pb.Admin, error) {
	admin := pb.Admin{}

	if err := a.DB.QueryRow(ctx, `select id, phone, login, password from admins where login = $1`, req.GetLogin()).Scan(
		&admin.Id,
		&admin.Phone,
		&admin.Login,
		&admin.Password,
	); err != nil {
		return nil, err
	}

	return &admin, nil
}

