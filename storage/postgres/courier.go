package postgres

import (
	"context"
	"fmt"
	pb "user_service/genproto/user_protos"
	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type courierRepo struct {
	DB *pgxpool.Pool
}

func NewCourierRepo (db *pgxpool.Pool) storage.ICourierStorage{
	return &courierRepo{
		DB: db,
	}
}

func (c *courierRepo) Create(ctx context.Context, createCourier *pb.CreateCourierRequest) (*pb.Courier, error) {
	var (
		id = uuid.New()
		courier = pb.Courier{}
	)

	query := `
		insert into couriers (id, first_name, last_name, phone, active, login, password, max_order_count, branch_id)
				values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
		returning id, first_name, last_name, phone, active::text, login, password, 
			max_order_count, branch_id, created_at::text `

	err := c.DB.QueryRow(ctx, query,
		 	id, 
			createCourier.GetFirstName(), 
			createCourier.GetLastName(),
			createCourier.GetPhone(),
			createCourier.GetActive(),
 			createCourier.GetLogin(),
			createCourier.GetPassword(),
			createCourier.GetMaxOrderCount(),
			createCourier.GetBranchId(),
		).Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.Active,
			&courier.Login,
			&courier.Password,
			&courier.MaxOrderCount,
			&courier.BranchId,
			&courier.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating Courier!", err.Error())
			return nil, err
	 	} 

		return &courier, nil
}

func (c *courierRepo) Get(ctx context.Context, key *pb.CourierPrimaryKey) (*pb.Courier, error) {
	var (
		courier = pb.Courier{}
	)

	query := `select id, first_name, last_name, phone, active::text, login,max_order_count, 
	branch_id, created_at::text, updated_at::text from couriers
			WHERE deleted_at = 0 and id = $1 `

	err := c.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Active,
		&courier.Login,
 		&courier.MaxOrderCount,
		&courier.BranchId,
		&courier.CreatedAt,
		&courier.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Courier by id!", err.Error())
		return nil, err
	}

	return &courier, nil
}

func (c *courierRepo) GetList(ctx context.Context, request *pb.GetCourierListRequest) (*pb.CouriersResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		couriers = pb.CouriersResponse{}
		filter, countQuery string
		search = request.Search
	)
	// search: firstname, lastname, phone

	countQuery = `select count(1) from couriers WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND first_name ilike '%%%s%%' OR last_name ilike '%%%s%%' OR phone ilike '%%%s%%' `,search, search, search)
	
	if search != ""{
		countQuery += filter
	}

	err := c.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of Couriers",err.Error())
		return nil, err
	}

	query := `select id, first_name, last_name, phone, active::text, login, max_order_count, 
	branch_id, created_at::text, updated_at::text from couriers
			WHERE deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := c.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting Couriers!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			courier = pb.Courier{}
		)

		err = rows.Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.Active,
			&courier.Login,
			&courier.MaxOrderCount,
			&courier.BranchId,
			&courier.CreatedAt,
			&courier.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning Couriers!",err.Error())
			return nil, err
		}
		couriers.Couriers = append(couriers.Couriers, &courier)
	}

	couriers.Count = count

	return &couriers, nil
}

func (c *courierRepo) Update(ctx context.Context, updCourier *pb.UpdateCourier) (*pb.Courier, error) {
	courier := pb.Courier{}

	query := `UPDATE couriers SET first_name = $1, last_name = $2, phone = $3, active = $4,
		max_order_count = $5, branch_id = $6, updated_at = now()
						WHERE deleted_at = 0 AND id = $7
		returning id, first_name, last_name, phone, active::text,
			max_order_count, branch_id, updated_at::text `

	err := c.DB.QueryRow(ctx, query,
		updCourier.GetFirstName(),
		updCourier.GetLastName(),
		updCourier.GetPhone(),
		updCourier.GetActive(),
		updCourier.GetMaxOrderCount(),
		updCourier.GetBranchId(),
		updCourier.GetId(),
	).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Active,
		&courier.MaxOrderCount,
		&courier.BranchId,
		&courier.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating Couriers!", err.Error())
		return nil, err
	}

	return &courier, nil
}

func (c *courierRepo) Delete(ctx context.Context, key *pb.CourierPrimaryKey) (error) {
	query := `UPDATE couriers set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := c.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting Courier by id!", err.Error())
		return err
	}
	return nil
}


// Change password!
func (c *courierRepo) GetPassword(ctx context.Context, request *pb.ChangeCourierPasswordRequest) (string,error) {
	query := `SELECT password from couriers 
		WHERE deleted_at = 0 AND login = $1 `
	
	var oldPassword string
	
	err := c.DB.QueryRow(ctx, query, request.Login).Scan(
		&oldPassword,
	)
	if err != nil{
		fmt.Println("Login is wrong !", err.Error())
		return "",err
	}

	return oldPassword,nil
}

func (c *courierRepo) UpdatePassword(ctx context.Context, login, password string) (string, error) {
	query := `UPDATE couriers SET password = $1 
			WHERE deleted_at = 0 AND login = $2
		returning password `
	

	err := c.DB.QueryRow(ctx, query, password, login).Scan(
		&password,
	)
	if err != nil{
		fmt.Println("Error while updating couriers password!", err.Error())
		return "", err
	}

	return password, nil
}


func (c *courierRepo) GetCourierByCredentials(ctx context.Context, req *pb.UserLoginRequest) (*pb.Courier, error) {
	courier := pb.Courier{}

	if err := c.DB.QueryRow(ctx, `select id, phone, login, password from couriers where login = $1`, req.GetLogin()).Scan(
		&courier.Id,
		&courier.Phone,
		&courier.Login,
		&courier.Password,
	); err != nil {
		return nil, err
	}

	return &courier, nil
}