DROP TYPE IF EXISTS client_discount_type_enum;
DROP TYPE IF EXISTS status_enum;

DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS branches;
DROP TABLE IF EXISTS couriers;
DROP TABLE IF EXISTS admins;
 