DO $$ 
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'client_discount_type_enum') THEN
        CREATE TYPE client_discount_type_enum AS ENUM ('sum', 'percent');
    END IF;
END $$;


DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'status_enum') THEN
        CREATE TYPE status_enum AS ENUM ('yes', 'no');
    END IF;
END $$;



CREATE TABLE IF NOT EXISTS clients (
    id uuid primary key,
    first_name varchar(40),
    last_name varchar(40),
    phone varchar(13),
    login varchar(30),
    password varchar(64),
    date_of_birth date,
    last_ordered_date TIMESTAMP,
    total_orders_sum numeric,
    total_orders_count int,
    discount_type client_discount_type_enum,
    discount_amount int,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS branches (
  id uuid primary key,
  name varchar(40),
  phone varchar(13),
  delivery_tarif_id uuid,
  work_hour_start varchar(5),
  work_hour_end varchar(5),
  address varchar(60),
  destination varchar(60),
  active status_enum,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS couriers (
    id uuid primary key,
    first_name varchar(40),
    last_name varchar(40),
    phone varchar(13),
    active status_enum,
    login varchar(30),
    password varchar(64),
    max_order_count int,
    branch_id uuid references branches(id),
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS admins (
  id uuid primary key,
  first_name varchar(30),
  last_name varchar(30),
  phone varchar(13),
  active status_enum,
  login varchar(30),
  password varchar(64),
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at int DEFAULT 0
);

 
 